import React from 'react';
import './PageLoader.css';

// TODO - replace with some fancy spinning thing
export default () => (
  <div className='pageLoader'>
    <span>Loading Parking Spots...</span>
  </div>
);
