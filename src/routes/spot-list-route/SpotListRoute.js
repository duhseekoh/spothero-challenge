import React, { Component } from "react";
import { getLocations } from "../../api/locations";
import "./SpotListRoute.css";
import PageLoader from "../../components/page-loader/PageLoader";
import SpotList from "./components/spot-list/SpotList";
import SpotMap from "./components/spot-map/SpotMap";
import SpotNav from "./components/spot-nav/SpotNav";
import SpotDetailModal from "./components/spot-detail-modal/SpotDetailModal";

/**
 * Page that displays the list of parking spot locations retrieved from a search.
 */
class SpotListRoute extends Component {
  constructor(props) {
    super(props);

    // TODO - use redux instead of component state
    this.state = {
      loading: false,
      locations: [],
      // TODO - would prefer an id be stored on each location. would use that
      // instead and would also grab the location out of redux based on that id
      selectedLocation: null,
      // TODO - populate from search criteria
      breadcrumbs: ["Chicago", "Millenium Park"]
    };
  }

  componentDidMount() {
    // TODO - add error handling
    // TODO - move to redux action instead
    this.setState({
      loading: true
    });
    getLocations().then(locations => {
      this.setState({
        locations,
        loading: false
      });
    });
  }

  /**
   * When the user selects a location in the list, it gets set here. The detail
   * modal will then render.
   */
  handleSelectLocation = selectedLocation => {
    this.setState({
      selectedLocation
    });
  };

  /**
   * Clearing out the selected location will close the detail modal.
   */
  handleCloseDetailModal = () => {
    this.setState({
      selectedLocation: null
    });
  };

  render() {
    const { breadcrumbs, loading, locations, selectedLocation } = this.state;

    if (loading) {
      return <PageLoader />;
    }

    return (
      <div className="spotListRoute">
        <div className="spotListRouteListHolder">
          <SpotNav breadcrumbs={breadcrumbs} locationCount={locations.length} />
          <SpotList
            locations={locations}
            onSelectLocation={this.handleSelectLocation}
          />
        </div>
        <div className="spotListRouteMapHolder">
          <SpotMap locations={locations} />
        </div>

        {selectedLocation !== null && (
          <SpotDetailModal
            location={selectedLocation}
            onClose={this.handleCloseDetailModal}
          />
        )}
      </div>
    );
  }
}

export default SpotListRoute;
