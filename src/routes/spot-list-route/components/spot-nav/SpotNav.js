import React from "react";
import "./SpotNav.css";

/**
 * Header section to display after a successful search.
 */
export default ({ breadcrumbs, locationCount }) => {
  if (!breadcrumbs || breadcrumbs.length === 0) {
    return null;
  }

  return (
    <div className="spotNav">
      {breadcrumbs.map((bc, i) => {
        if (i === breadcrumbs.length - 1) {
          return <span className='spotNavBreadcrumbLastEntry'>{bc}</span>;
        }

        return (
          <span>
            <a href='#'>{bc}</a>{' '}&gt;{' '}
          </span>

        )
      })}
      <h1 className='spotNavTitle'>{breadcrumbs[breadcrumbs.length - 1]}</h1>
      <div className='spotNavLocationCount'>{locationCount} Spots Available</div>
    </div>
  );
};
