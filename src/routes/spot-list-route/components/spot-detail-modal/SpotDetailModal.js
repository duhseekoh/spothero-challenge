import React from "react";
import "./SpotDetailModal.css";

// TODO - when adding another modal, abstract into a common modal component
// where contents can be supplied to it.
// TODO - use a react portal to display modals as direct child of <App> or body
export default ({ location, onClose }) => (
  <div className="modalOverlay" onClick={onClose}>
    <div className="spotDetailModal" onClick={e => e.stopPropagation()}>
      <div className="spotDetailModalShadowLayer">
        <header className="spotDetailModalHeader">
          <span>SPOT DETAILS</span>
          {/* TODO replace X with an icon */}
          <button
            className="spotDetailModalCloseButton"
            type="input"
            onClick={onClose}
          >
            X
          </button>
        </header>
        <div className="spotDetailModalBody">
          <div className="spotDetailModalLocationTitle">{location.title}</div>
          <p>{location.description}</p>
        </div>
      </div>
    </div>
  </div>
);
