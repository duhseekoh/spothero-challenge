import React from "react";
import './SpotList.css';

/**
 * TODO - would most likely keep this in a separate file if there were anymore
 * complexity to SpotList
 */
const ListItem = ({ location, onSelectLocation }) => (
  <div className='spotListItem'>
    <div className='spotListItemImageHolder'>
      <img src={location.image_url} alt={location.title} />
    </div>
    <div className='spotListItemDetailsHolder'>
      <div className='spotListItemDetailsTitle'>{location.title}</div>
      <div className='spotListItemDetailsDistance'>{location.distance}</div>
      <div className='spotListItemDetailsLinkHolder'>
        <a href="#" onClick={() => onSelectLocation(location)}>
          Details
        </a>
      </div>
    </div>

  </div>
);

/**
 * Scrollable list of spot locations
 */
export default ({ locations, onSelectLocation }) => (
  <div className='spotList'>
    { locations.map(l => <ListItem location={l} onSelectLocation={onSelectLocation} /> ) }
  </div>
);

// TODO - add prop-types
