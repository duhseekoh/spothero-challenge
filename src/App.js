import React, { Component } from 'react';
import SpotListRoute from './routes/spot-list-route/SpotListRoute';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app">
        {/* TODO - use react-router to hook up routes to browser locations */}
        <SpotListRoute />
      </div>
    );
  }
}

export default App;
