/**
 * Fetch parking spot locations
 * TODO - would parameterize with user's address and other search preferences
 */
export const getLocations = async () => {
  const resp = await fetch('./mock-data/locations.json');
  // simulate network delay
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(resp.json());
    }, 400);
  });
};
