Dom DiCicco's submission for the SpotHero take home tech project.

## Getting Started
This is a create-react-app of the un-ejected nature.
1. Install Modules `yarn`
1. Boot it up `yarn start`

## More Architecture
If I were really building this thing, I'd expand the SPA architecture to include
the following.

### redux
for state management
follow redux module approach (/redux/module-name/actions.js|reducer.js|selectors.js)
store all entities retrieved through api calls
store some level of ui state (e.g. loading, saving, what entities to display on what pages)

### redux-thunk
to handle side effects

### flow
provide typings for all component props
add a types.js file in each redux module

### prop-types
instead of prop-types, use flow to generate prop-types based on flow types.

### react-router
build out a pages folder structure
container components can be the screens themselves, which can contain other container components

### api client
start with an api folder which contains methods for each api call, configurable with params
separate that out into a separate api client package after a while

### redux-form
for anything more complex than a couple of text inputs

### css modules or jss
i'm scoping my css by just creating long class names specific to each component.
this is better accomplished by using css modules to scope or use jss to create styles with js object notation and apply to each component

### ui framework
material ui or semantic ui
depending on designer resources, just use material ui out of the box with some theming.

### jest
for unit and component testing
test redux modules and react components

### prettier && eslint
automatic file formatting that finally works
